import {useState} from 'react';
import moment from 'moment';

const Calendar =()=>{

  const [getMoment, setMoment]=useState(moment());

  const today = getMoment;
  const firstWeek = today.clone().startOf('month').week();
  const lastWeek = today.clone().endOf('month').week() === 1 ? 53 : today.clone().endOf('month').week();

    const calendarArr=()=>{

      let result = [];
      let week = firstWeek;
      for ( week; week <= lastWeek; week++) {
        result = result.concat(
          <tr key={week}>
            {
              Array(7).fill(0).map((data, index) => {
                let days = today.clone().startOf('year').week(week).startOf('week').add(index, 'day'); //d로해도되지만 직관성

                if(moment().format('YYYYMMDD') === days.format('YYYYMMDD')){
                  return(
                      <td key={index} style={{backgroundColor:'#38a6ff'}} >
                        <div className="date-frame">
                          <div className="date-top">{days.format('D')}</div>
                        </div>
                      </td>
                  );
                }else if(days.format('MM') !== today.format('MM')){
                  return(
                      <td key={index} style={{opacity: '0.5'}} >
                        <div className="date-frame">
                          <div className="date-top">{days.format('D')}</div>
                        </div>
                      </td>
                  );
                }else{
                  return(
                      <td key={index}  >
                        <div className="date-frame">
                          <div className="date-top">{days.format('D')}</div>
                        </div>
                      </td>
                  );
                }
              })
            }
          </tr>
        );
      }
      return result;
    }

  return (
    <div className="App">

        <div className="control">
          <button onClick={()=>{ setMoment(getMoment.clone().subtract(1, 'month')) }} >이전달</button>
          <span>{today.format('YYYY 년 MM 월')}</span>
          <button onClick={()=>{ setMoment(getMoment.clone().add(1, 'month')) }} >다음달</button>
        </div>
        <table className="table calendar-table">
          <thead>
            <tr>
              <th>Sun</th>
              <th>Mon</th>
              <th>Tue</th>
              <th>Wen</th>
              <th>Thu</th>
              <th>Fri</th>
              <th>Sat</th>
            </tr>
          </thead>
          <tbody>
            {calendarArr()}
          </tbody>
        </table>
    </div>
  );
}
export default Calendar;