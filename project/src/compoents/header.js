import React, { PureComponent } from 'react'
import {useState} from 'react';
import moment from 'moment';

const Header =() => {
  const [getMoment, setMoment]=useState(moment());     
  const today = getMoment;    // today == moment()   입니다.

  return(
    <div className="header">
        <div className="control">
          <button>이전달</button>
          <span>{today.format('YYYY 년 MM 월')}</span> 
          <button>다음달</button>
        </div>
    </div>
  );
}
export default Header