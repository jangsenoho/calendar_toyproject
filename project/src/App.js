import React, { Fragment, PureComponent } from 'react'
import Header from './compoents/header';
import Main from './compoents/Calendar';

import './assets/css/styles.css';

class App extends PureComponent {

  render() {
    return (
      <Fragment>
        <div className="section">
          <div className="wrapper">
          <Main/>
          </div>
        </div>
      </Fragment>
    )
  }
}

export default App